import * as React from "react";
import Link from "gatsby-link";
import Helmet from "react-helmet";

// import("jquery/dist/jquery.js")
// import("popper.js/dist/umd/popper.js")
// import("bootstrap/dist/js/bootstrap.js")

interface DefaultLayoutProps extends React.HTMLProps<HTMLDivElement> {
  location: {
    pathname: string;
  };
  children: any;
}

class DefaultLayout extends React.PureComponent<DefaultLayoutProps, void> {
  componentDidMount() {
    import("jquery/dist/jquery.js")
      .then(function() {
        return import("popper.js/dist/umd/popper.js");
      })
      .then(function() {
        return import("bootstrap/dist/js/bootstrap.js");
      });
  }

  public render() {
    return (
      <div>
        <Helmet
          title="Blessing & Emeka Are Getting Married"
          meta={[
            { charset: "UTF-8" },
            {
              name: "viewport",
              content: "width = device-width, initial-scale=1"
            },
            { httpEquiv: "X-UA-COMPATIBLE", content: "IE=edge" }
          ]}
        >
          <link
            rel="preload"
            href="https://fonts.googleapis.com/css?family=Josefin+Sans"
            as="style"
            onload="this.onload=null;this.rel='stylesheet'"
          />
          <noscript
          >{`<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Josefin+Sans"></link>`}</noscript>
          <script
            defer
            src="https://use.fontawesome.com/releases/v5.0.9/js/all.js"
            integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl"
            crossOrigin="anonymous"
          />
          <script>
            {`/*! loadCSS. [c]2017 Filament Group, Inc. MIT License */
          !function(t){"use strict";t.loadCSS||(t.loadCSS=function(){});var e=loadCSS.relpreload={};if(e.support=function(){var e;try{e=t.document.createElement("link").relList.supports("preload")}catch(t){e=!1}return function(){return e}}(),e.bindMediaToggle=function(t){var e=t.media||"all";function a(){t.media=e}t.addEventListener?t.addEventListener("load",a):t.attachEvent&&t.attachEvent("onload",a),setTimeout(function(){t.rel="stylesheet",t.media="only x"}),setTimeout(a,3e3)},e.poly=function(){if(!e.support())for(var a=t.document.getElementsByTagName("link"),n=0;n<a.length;n++){var o=a[n];"preload"!==o.rel||"style"!==o.getAttribute("as")||o.getAttribute("data-loadcss")||(o.setAttribute("data-loadcss",!0),e.bindMediaToggle(o))}},!e.support()){e.poly();var a=t.setInterval(e.poly,500);t.addEventListener?t.addEventListener("load",function(){e.poly(),t.clearInterval(a)}):t.attachEvent&&t.attachEvent("onload",function(){e.poly(),t.clearInterval(a)})}"undefined"!=typeof exports?exports.loadCSS=loadCSS:t.loadCSS=loadCSS}("undefined"!=typeof global?global:this);`}
          </script>
        </Helmet>
        {this.props.children()}
      </div>
    );
  }
}

export default DefaultLayout;

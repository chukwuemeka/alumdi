import * as React from "react";
import Link from "gatsby-link";
import { compose, withHandlers, withState } from "recompose";
import styled from "styled-components";
import { css } from "styled-components";
import Navbar from "../components/Navbar";
import Gallery from "../components/Gallery";
import Chapter from "../components/Chapter";
import Img from "gatsby-image";
import mixins from "../styles/mixins";
import "../styles/index.scss";
var loremIpsum = require("lorem-ipsum");
var banner = require("../img/banner.jpg");
var californiaAccomodations = require("../pdf/california_accomodations.pdf");
declare var $: any;

// Please note that you can use https://github.com/dotansimha/graphql-code-generator
// to generate all types from graphQL schema
interface Props {
  className: string;
}

interface EnhancedProps extends Props {
  data: {
    bannerImage: any;
    churchImage: any;
    nigerianWeddingImage: any;
    weddingReceptionImage: any;
    ourStoryImage0: any;
    ourStoryImage1: any;
    ourStoryImage2: any;
    ourStoryImage3: any;
    phuketImage: any;
    hongKongImage: any;
    mumbaiImage: any;
    kathmanduImage: any;
    site: {
      siteMetadata: {
        title: string;
      };
    };
  };
}

const enhance = compose<EnhancedProps, Props>(
  withState("doShowImageSlider", "setDoShowImageSlider", false)
);

const Index: React.SFC<EnhancedProps> = function({ className, data }) {
  const galleryImages = Array.from(Array(18).keys()).map(function(
    index: number
  ) {
    return (data as any)[`galleryImage${index}`];
  });
  return (
    <div className={className}>
      <Navbar />
      <section className="banner d-flex justify-content-center align-items-center">
        <div className="dimmer" />
        <div className="jumbotron jumbotron-fluid">
          <div className="container">
            <div className="row">
              <div className="col-12 text-center">
                <h1 className="display-4">Blessing & Emeka</h1>
                <p className="lead">Are getting married on August 18, 2018</p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section id="our-story" className="jumbotron jumbotron-fluid our-story">
        <div className="container">
          <div className="row">
            <div className="col-12 text-center">
              <h2 className="title text-uppercase">Our Story</h2>
              <hr className="my-4" />
            </div>
          </div>
          <div className="chapters">
            <div className="timeline-start" />
            <div className="timeline" />
            <div className="timeline-end" />
            <div className="row align-items-center justify-content-center chapter pt-0">
              <div className="col-md-6 col-lg-5 text-center text-md-left text-container">
                <h5 className="subtitle text-uppercase">
                  Where it all started
                </h5>
                <hr className="d-none d-md-block ml-0" />
                <p>
                  I was hosting a fight watch party for UFC 198.
                  Blessing had decided to attend my party after a lukewarm date.
                  I saw Blessing keeping to herself in the corner, so naturally
                  I struck up a loud and obnoxious conversation with her
                  (clearly a good flirting technique). Little did we know this
                  would be the first day of the rest of our lives.
                </p>
              </div>
              <div className="col-md-6 col-lg-5 img-container">
                <Img sizes={data.ourStoryImage0.sizes} />
              </div>
            </div>
            <div className="row align-items-center justify-content-center chapter">
              <div className="col-md-6 col-lg-5 text-center text-md-left text-container">
                <h5 className="subtitle text-uppercase">The Dating Game</h5>
                <hr className="d-none d-md-block ml-0" />
                <p>
                  After the party, I sent Blessing a message asking her to grab
                  a drink with me and she said yes! This was the first of many
                  yeses. The date went well -- the night was beautiful, the Thai
                  place we ducked into had delicious food, and the best part was
                  how our conversation flowed. One date became two and two
                  became three.
                </p>
              </div>
              <div className="col-md-6 col-lg-5 img-container">
                <Img sizes={data.ourStoryImage1.sizes} />
              </div>
            </div>
            <div className="row align-items-center justify-content-center chapter">
              <div className="col-md-6 col-lg-5 text-center text-md-left text-container">
                <h5 className="subtitle text-uppercase">
                  Unlimited Adventures
                </h5>
                <hr className="d-none d-md-block ml-0" />
                <p>
                  As the dating continued, each day together became our new
                  favorite day. We strived to encounter new things and as a
                  result we created tons of great memories together. Some of our
                  favorite shared memories was going parasailing in Puerto Rico,
                  camping in Montreal, exploring rainforests, chatting up some
                  kooky people, snoozing on spectacular beaches and the list
                  goes on.
                </p>
              </div>
              <div className="col-md-6 col-lg-5 img-container">
                <Img sizes={data.ourStoryImage2.sizes} />
              </div>
            </div>
          </div>
          <div className="row d-flex align-items-center justify-content-center">
            <div className="col-12 col-lg-8">
              <div className="row question">
                <div className="col-12 text-center">
                  <h5 className="subtitle text-uppercase">The Question</h5>
                  <hr className="d-none d-md-block" />
                  <p>
                    Well the more time Blessing and I spent together, the more I
                    knew she was the one I wanted to spend the rest of my life
                    with. She is a very smart lady, so I knew I had to be clever
                    about arranging the proposal without tipping her off.
                    Blessing's birthday is in November and she loves the holiday
                    season, so I planned a road trip to Toronto where we could
                    check out the city's annual Christmas festival. I found the
                    perfect ring for her and quickly after this I chose the
                    special day. November 17th was the day our lives changed
                    forever. After dinner, we walked over to the Christmas
                    festival. While we stood in front of a 50 foot spruce tree
                    adorned with ornaments, I got down on one knee, proposed to
                    her, and she said yes.
                  </p>
                </div>
                <div className="col-12">
                  <Img sizes={data.ourStoryImage3.sizes} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section id="events" className="jumbotron jumbotron-fluid events">
        <div className="container">
          <div className="row">
            <div className="col-12 text-center">
              <h2 className="title text-uppercase">Events</h2>
              <hr className="my-4" />
            </div>
          </div>
          <div className="row">
            <div className="col-md-4 text-center event">
              <div className="">
                <div>
                  <Img sizes={data.nigerianWeddingImage.sizes} />
                </div>
                <h6 className="subtitle my-3 text-uppercase">
                  Traditional Ceremony
                </h6>
                <p className="">
                  6:00 PM <br />
                  June 30, 2018 <br />
                  Best Western Plus <br />
                  1100 Cromwell Bridge Rd, Towson, MD 21286 <br />
                </p>
              </div>
            </div>
            <div className="col-md-4 text-center event">
              <div className="">
                <Img sizes={data.churchImage.sizes} />
                <h6 className="subtitle my-3 text-uppercase">Main Ceremony</h6>
                <p className="">
                  2:00 PM <br />
                  August 18, 2018 <br />
                  St. Kilian Catholic Church <br />
                  26872 Estanciero Drive, Mission Viejo, CA 92691 <br />
                </p>
                <a href={californiaAccomodations} download>
                  <button className="btn btn-info">Accomodations</button>
                </a>
              </div>
            </div>
            <div className="col-md-4 text-center event">
              <div className="">
                <Img sizes={data.weddingReceptionImage.sizes} />
                <h6 className="subtitle my-3 text-uppercase">Reception</h6>
                <p className="">
                  5:00 PM <br />
                  August 18, 2018 <br />
                  Cannons Seafood Grill <br />
                  34344 Street of the Green Lantern, Dana Point, CA 92629 <br />
                </p>
                <a href={californiaAccomodations} download>
                  <button className="btn btn-info">Accomodations</button>
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section id="gallery" className="jumbotron jumbotron-fluid gallery">
        <div className="container">
          <div className="row">
            <div className="col-12 text-center">
              <h2 className="title text-uppercase">Gallery</h2>
              <hr className="my-4" />
            </div>
          </div>
          <div className="row align-items-center justify-content-center">
            <div className="col-12 col-md-8">
              <Gallery images={galleryImages} />
            </div>
          </div>
        </div>
      </section>
      <section id="honeymoon" className="jumbotron jumbotron-fluid honeymoon">
        <div className="container">
          <div className="row">
            <div className="col-12 text-center">
              <h2 className="title text-uppercase">Honeymoon</h2>
              <hr className="my-4" />
            </div>
          </div>
          <div className="row my-2">
            <div className="col-6 col-md-3 text-center">
              <div className="destination">
                <div>
                  <Img sizes={data.phuketImage.sizes} />
                </div>
                <h6 className="subtitle my-3 text-uppercase">Phuket</h6>
              </div>
            </div>
            <div className="col-6 col-md-3 text-center">
              <div className="destination">
                <div>
                  <Img sizes={data.hongKongImage.sizes} />
                </div>
                <h6 className="subtitle my-3 text-uppercase">Hong Kong</h6>
              </div>
            </div>
            <div className="col-6 col-md-3 text-center">
              <div className="destination">
                <div>
                  <Img sizes={data.mumbaiImage.sizes} />
                </div>
                <h6 className="subtitle my-3 text-uppercase">Mumbai</h6>
              </div>
            </div>
            <div className="col-6 col-md-3 text-center">
              <div className="destination">
                <div>
                  <Img sizes={data.kathmanduImage.sizes} />
                </div>
                <h6 className="subtitle my-3 text-uppercase">Kathmandu</h6>
              </div>
            </div>
          </div>
          <div className="row align-items-center justify-content-center">
            <div className="col-8 col-sm-6 col-lg-4 text-center">
              <p>
                We’ve lived together quite a while with all our pots and pans,
                and as we don’t need homely gifts we’ve got another plan!
              </p>
              <p>
                We know it’s not traditional but an awful lot more fun, to have
                items on our wedding list to help us catch some sun!
              </p>
              <p>
                So if you’d like to give a gift and send us on our way a
                donation to our honeymoon would really make our day!!
              </p>
            </div>
          </div>
          <div className="row align-items-center justify-content-center">
            <div className="col-12 text-center">
              <a href="https://registry.theknot.com/blessing-nicholas-emeka-ezekwe-august-2018-ca/24595694">
                <button className="btn btn-info">Continue to Registry</button>
              </a>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};
const StyledIndex = styled(Index)`
  .jumbotron {
    margin-bottom: 0;
  }
  .gallery {
    background: none;
  }
  .honeymoon,
  .events,
  .our-story,
  .gallery {
    hr {
      border-top: 3px solid black;
      width: 25px;
    }
    .title {
      letter-spacing: 5px;
      ${mixins.mediaBreakpointDownSmall`
      letter-spacing: 0px;
    `};
    }
    .subtitle {
      letter-spacing: 0px;
      ${mixins.mediaBreakpointDownSmall`
      letter-spacing: 0px;
    `};
    }
  }
  .honeymoon {
    background: #f2f2f2;
    .subtitle {
      font-weight: 650;
      letter-spacing: 2px;
    }
    .gatsby-image-outer-wrapper {
      height: 143px;
      overflow: hidden;
      ${mixins.mediaBreakpointDownLarge`
        height: 120px;
    `};
      ${mixins.mediaBreakpointDownMedium`
        height: 85px;
    `};
      ${mixins.mediaBreakpointDownSmall`
        height: 130px;
    `};
      ${mixins.mediaBreakpointDownExtraSmall`
        height: 90px;
    `};
    }
  }
  .events {
    background: #f2f2f2;
    .event {
      ${mixins.mediaBreakpointDownSmall`
        padding-bottom: 2rem;
        &:last-child {
          padding-bottom: 0;
        }
    `};
    }
    .subtitle {
      font-weight: 650;
      letter-spacing: 2px;
    }
    .gatsby-image-outer-wrapper {
      height: 190px;
      overflow: hidden;
      ${mixins.mediaBreakpointDownLarge`
        height: 163px;
    `};
      ${mixins.mediaBreakpointDownMedium`
        height: 118px;
    `};
      ${mixins.mediaBreakpointDownSmall`
        height: 283px;
    `};
      ${mixins.mediaBreakpointDownExtraSmall`
        height: 192px;
    `};
    }
  }
  .our-story {
    background: none;
    ${mixins.mediaBreakpointDownSmall`
        .subtitle {
          padding-top: 0.5rem;
          padding-bottom: 0.5rem;
        }
    `};
    .question {
      ${mixins.mediaBreakpointDownSmall`
        flex-direction: column-reverse;
    `};
    }
    .timeline {
      position: absolute;
      border-left: 1px solid #f2f4f4;
      height: 100%;
      margin-left: 50%;
      ${mixins.mediaBreakpointDownSmall`
        display: none;
      `};
    }
    .timeline-start {
      position: absolute;
      background: #f2f4f4;
      width: 14px;
      height: 14px;
      border-radius: 50%;
      top: 0;
      left: -7px;
      margin-left: 50%;
      ${mixins.mediaBreakpointDownSmall`
        display: none;
      `};
    }
    .timeline-end {
      position: absolute;
      background: #f2f4f4;
      width: 14px;
      height: 14px;
      border-radius: 50%;
      bottom: 0;
      left: -7px;
      margin-left: 50%;
      ${mixins.mediaBreakpointDownSmall`
        display: none;
      `};
    }
    .chapters {
      position: relative;
      margin-top: 1.5rem;
      margin-bottom: 1.5rem;
      ${mixins.mediaBreakpointDownSmall`
        margin-top: 0;
        margin-bottom: 0;
      `};
    }
    .chapter {
      padding-top: 50px;
      padding-bottom: 50px;
      ${mixins.mediaBreakpointDownSmall`
        padding-top: 15px;
        padding-bottom: 15px;
      `};
      &:nth-child(1) {
        font-size: 550rem;
      }
      &:nth-child(2n) {
        ${mixins.mediaBreakpointDownSmall`
          flex-direction: column-reverse;
        `};
        .img-container {
          padding-left: 40px;
          ${mixins.mediaBreakpointDownSmall`
            padding-left: 15px;
            padding-right: 15px;
          `};
        }
        .text-container {
          padding-right: 40px;
          ${mixins.mediaBreakpointDownSmall`
            padding-left: 15px;
            padding-right: 15px;
          `};
        }
      }
      &:nth-child(2n + 1) {
        flex-direction: row-reverse;
        ${mixins.mediaBreakpointDownSmall`
          flex-direction: column-reverse;
        `};
        .img-container {
          padding-right: 40px;
          ${mixins.mediaBreakpointDownSmall`
            padding-left: 15px;
            padding-right: 15px;
          `};
        }
        .text-container {
          padding-left: 40px;
          ${mixins.mediaBreakpointDownSmall`
            padding-left: 15px;
            padding-right: 15px;
          `};
        }
      }
    }
  }
  .banner {
    color: white;
    background-image: ${function(props: EnhancedProps) {
      return `url(${props.data.bannerImage.sizes.src})`;
    }};
    ${function(props: EnhancedProps) {
      return props.data.bannerImage.sizes.srcSet
        .split(",")
        .reverse()
        .reduce(function(acc: string, line: string) {
          const toks: string[] = line.split(" ");
          const url = toks[0];
          const size = toks[1].replace("w", "px");
          if (parseInt(size.replace("px", "w")) < 800) {
            return acc;
          }
          return acc + `
          @media (max-width: ${size}) { 
            background-image: url(${url}) 
          }
        `;
        }, "");
    }} background-repeat: no-repeat;
    background-position: center center;
    background-size: cover;
    min-height: 50vw;
    position: relative;
    .dimmer {
      position: absolute;
      background-color: rgba(0, 0, 0, 0.325);
      width: 100%;
      height: 100%;
      top: 0;
      left: 0;
      z-index: 1;
    }
    .jumbotron {
      position: relative;
      z-index: 2;
      background: none;
      .display-4 {
        text-transform: uppercase;
        letter-spacing: 10px;
        font-weight: 450;
        ${mixins.mediaBreakpointDownLarge`
            /* letter-spacing: 1.25rem; */
            /* font-size: 3em; */
        `};
        ${mixins.mediaBreakpointDownMedium`
            /* letter-spacing: 1rem; */
            /* font-size: 2.625em; */
        `};
        ${mixins.mediaBreakpointDownSmall`
            letter-spacing: 0px;
            /* font-size: 3em; */
        `};
        ${mixins.mediaBreakpointDownExtraSmall`
            /* letter-spacing: 1rem; */
            /* font-size: 3.5em; */
        `};
      }
    }
    ${mixins.mediaBreakpointDownMedium`
      /* min-height: 25vw; */
    `};
    ${mixins.mediaBreakpointDownSmall`
      /* min-height: 17.5vw; */
    `};
  }
`;

export default enhance(StyledIndex);

export const query = graphql`
  query IndexQuery {
    site {
      siteMetadata {
        title
      }
    }
    nigerianWeddingImage: imageSharp(id: { regex: "/nigerian_wedding.png/" }) {
      sizes(maxWidth: 360) {
        ...GatsbyImageSharpSizes
      }
    }
    bannerImage: imageSharp(id: { regex: "/banner.jpg/" }) {
      sizes(maxWidth: 1600) {
        ...GatsbyImageSharpSizes
      }
    }
    churchImage: imageSharp(id: { regex: "/church.jpg/" }) {
      sizes(maxWidth: 360) {
        ...GatsbyImageSharpSizes
      }
    }
    weddingReceptionImage: imageSharp(
      id: { regex: "/wedding_reception.jpg/" }
    ) {
      sizes(maxWidth: 360) {
        ...GatsbyImageSharpSizes
      }
    }

    phuketImage: imageSharp(id: { regex: "/phuket.jpg/" }) {
      sizes(maxWidth: 360) {
        ...GatsbyImageSharpSizes
      }
    }
    hongKongImage: imageSharp(id: { regex: "/hong_kong.jpg/" }) {
      sizes(maxWidth: 360) {
        ...GatsbyImageSharpSizes
      }
    }
    mumbaiImage: imageSharp(id: { regex: "/mumbai.jpg/" }) {
      sizes(maxWidth: 360) {
        ...GatsbyImageSharpSizes
      }
    }
    kathmanduImage: imageSharp(id: { regex: "/kathmandu.jpg/" }) {
      sizes(maxWidth: 360) {
        ...GatsbyImageSharpSizes
      }
    }

    ourStoryImage0: imageSharp(id: { regex: "/our_story/0.jpg/" }) {
      sizes(maxWidth: 960) {
        ...GatsbyImageSharpSizes
      }
    }
    ourStoryImage1: imageSharp(id: { regex: "/our_story/1.jpg/" }) {
      sizes(maxWidth: 960) {
        ...GatsbyImageSharpSizes
      }
    }
    ourStoryImage2: imageSharp(id: { regex: "/our_story/2.jpg/" }) {
      sizes(maxWidth: 960) {
        ...GatsbyImageSharpSizes
      }
    }
    ourStoryImage3: imageSharp(id: { regex: "/our_story/3.jpg/" }) {
      sizes(maxWidth: 1600) {
        ...GatsbyImageSharpSizes
      }
    }

    galleryImage0: imageSharp(id: { regex: "/gallery/0.jpg/" }) {
      sizes(maxWidth: 1920) {
        ...GatsbyImageSharpSizes
      }
    }
    galleryImage1: imageSharp(id: { regex: "/gallery/1.jpg/" }) {
      sizes(maxWidth: 1920) {
        ...GatsbyImageSharpSizes
      }
    }
    galleryImage2: imageSharp(id: { regex: "/gallery/2.jpg/" }) {
      sizes(maxWidth: 1920) {
        ...GatsbyImageSharpSizes
      }
    }
    galleryImage3: imageSharp(id: { regex: "/gallery/3.jpg/" }) {
      sizes(maxWidth: 1920) {
        ...GatsbyImageSharpSizes
      }
    }
    galleryImage4: imageSharp(id: { regex: "/gallery/4.jpg/" }) {
      sizes(maxWidth: 1920) {
        ...GatsbyImageSharpSizes
      }
    }
    galleryImage5: imageSharp(id: { regex: "/gallery/5.jpg/" }) {
      sizes(maxWidth: 1920) {
        ...GatsbyImageSharpSizes
      }
    }
    galleryImage6: imageSharp(id: { regex: "/gallery/6.jpg/" }) {
      sizes(maxWidth: 1920) {
        ...GatsbyImageSharpSizes
      }
    }
    galleryImage7: imageSharp(id: { regex: "/gallery/7.jpg/" }) {
      sizes(maxWidth: 1920) {
        ...GatsbyImageSharpSizes
      }
    }
    galleryImage8: imageSharp(id: { regex: "/gallery/8.jpg/" }) {
      sizes(maxWidth: 1920) {
        ...GatsbyImageSharpSizes
      }
    }
    galleryImage9: imageSharp(id: { regex: "/gallery/9.jpg/" }) {
      sizes(maxWidth: 1920) {
        ...GatsbyImageSharpSizes
      }
    }
    galleryImage10: imageSharp(id: { regex: "/gallery/10.jpg/" }) {
      sizes(maxWidth: 1920) {
        ...GatsbyImageSharpSizes
      }
    }
    galleryImage11: imageSharp(id: { regex: "/gallery/11.jpg/" }) {
      sizes(maxWidth: 1920) {
        ...GatsbyImageSharpSizes
      }
    }
    galleryImage12: imageSharp(id: { regex: "/gallery/12.jpg/" }) {
      sizes(maxWidth: 1920) {
        ...GatsbyImageSharpSizes
      }
    }
    galleryImage13: imageSharp(id: { regex: "/gallery/13.jpg/" }) {
      sizes(maxWidth: 1920) {
        ...GatsbyImageSharpSizes
      }
    }
    galleryImage14: imageSharp(id: { regex: "/gallery/14.jpg/" }) {
      sizes(maxWidth: 1920) {
        ...GatsbyImageSharpSizes
      }
    }
    galleryImage15: imageSharp(id: { regex: "/gallery/15.jpg/" }) {
      sizes(maxWidth: 1920) {
        ...GatsbyImageSharpSizes
      }
    }
    galleryImage16: imageSharp(id: { regex: "/gallery/16.jpg/" }) {
      sizes(maxWidth: 1920) {
        ...GatsbyImageSharpSizes
      }
    }
    galleryImage17: imageSharp(id: { regex: "/gallery/17.jpg/" }) {
      sizes(maxWidth: 1920) {
        ...GatsbyImageSharpSizes
      }
    }
    galleryImage18: imageSharp(id: { regex: "/gallery/18.jpg/" }) {
      sizes(maxWidth: 1920) {
        ...GatsbyImageSharpSizes
      }
    }
    galleryImage19: imageSharp(id: { regex: "/gallery/19.jpg/" }) {
      sizes(maxWidth: 1920) {
        ...GatsbyImageSharpSizes
      }
    }
  }
`;

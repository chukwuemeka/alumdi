import * as React from "react";
import styled from "styled-components";
import Img from "gatsby-image";
import mixins from "../styles/mixins";
declare var lightGallery: any;

// Please note that you can use https://github.com/dotansimha/graphql-code-generator
// to generate all types from graphQL schema
interface Props extends React.HTMLProps<HTMLDivElement> {
  images: any[];
}

interface EnhancedProps extends Props {
  className: string;
}

class Gallery extends React.PureComponent<Props, any> {
  private lightGallery: any;

  constructor(props: Props) {
    super(props);
    this.lightGallery = undefined;
    this.onLightGallery = this.onLightGallery.bind(this);
  }

  onLightGallery(ref: any) {
    this.lightGallery = ref;
  }

  componentDidMount() {
    const component = this;
    import("picturefill/dist/picturefill.js")
      .then(function() {
        return import("lightgallery.js/dist/js/lightgallery.js");
      })
      .then(function() {
        return import("lightgallery.js/demo/js/lg-thumbnail.js");
      })
      .then(function() {
        return import("lightgallery.js/demo/js/lg-share.js");
      })
      .then(function() {
        return import("lightgallery.js/demo/js/lg-hash.js");
      })
      .then(function() {
        lightGallery(component.lightGallery, {
          selector: ".card",
          thumbnail: true,
          exThumbImage: "data-exthumbimage"
        });
      });
  }

  componentDidUpdate() {}

  public render() {
    const { className, images } = this.props;
    return (
      <div className={`card-columns ${className}`} ref={this.onLightGallery}>
        {images.map(function(image: any, index: number) {
          return (
            <div
              key={index}
              className="card rounded-0"
              data-exthumbimage={image.sizes.srcSet.split(",")[0].split(" ")[0]}
              data-src={image.sizes.src}
              data-srcset={image.sizes.srcSet}
              data-sizes={image.sizes.sizes}
            >
              <div className="d-flex justify-content-center align-items-center dimmer">
                <span className="fas fa-search" />
              </div>
              <Img className="card-img rounded-0" sizes={image.sizes} />
            </div>
          );
        })}
      </div>
    );
  }
}

export default styled(Gallery)`
  color: white;
  column-count: 3;
  .card {
    cursor: pointer;
    &:hover {
      .dimmer {
        background-color: rgba(0, 0, 0, 0.325);
        .fa-search {
          color: white;
        }
      }
    }
    .dimmer {
      position: absolute;
      background-color: rgba(0, 0, 0, 0);
      width: 100%;
      height: 100%;
      top: 0;
      left: 0;
      z-index: 1;
      transition: background-color 0.2s ease;
      .fa-search {
        color: rgba(0, 0, 0, 0);
        transition: color 0.2s ease;
      }
    }
  }
`;

import * as React from "react";
import Link from "gatsby-link";
import { compose } from "recompose";
import styled from "styled-components";
import mixins from "../styles/mixins";
import Img from "gatsby-image";

// Please note that you can use https://github.com/dotansimha/graphql-code-generator
// to generate all types from graphQL schema
interface Props {
  className: string;
  title: string;
  text: string;
  image: any;
}

interface EnhancedProps extends Props {}

const enhance = compose<EnhancedProps, Props>();

const Chapter: React.SFC<EnhancedProps> = function({
  className,
  title,
  text,
  image
}) {
  return (
    <div
      className={`row align-items-center justify-content-center ${className}`}
    >
      <div className="col-md-6 col-lg-5 text-left text-container">
        <h4 className="subtitle text-uppercase">{title}</h4>
        <hr className="ml-0" />
        <p>{text}</p>
      </div>
      <div className="col-md-6 col-lg-5 img-container">
        <Img sizes={image.sizes} />
      </div>
    </div>
  );
};
const StyledChapter = styled(Chapter)`
  padding: 50px 0;
  &:nth-child(2n) {
    .img-container {
      padding-left: 40px;
      ${mixins.mediaBreakpointDownSmall`
            padding-left: 0px;
          `};
    }
    .text-container {
      padding-right: 40px;
      ${mixins.mediaBreakpointDownSmall`
            padding-right: 0px;
          `};
    }
  }
  &:nth-child(2n + 1) {
    flex-direction: row-reverse;
    .img-container {
      padding-right: 40px;
      ${mixins.mediaBreakpointDownSmall`
            padding-right: 0px;
          `};
    }
    .text-container {
      padding-left: 40px;
      ${mixins.mediaBreakpointDownSmall`
            padding-left: 0px;
          `};
    }
  }
`;

export default enhance(StyledChapter);

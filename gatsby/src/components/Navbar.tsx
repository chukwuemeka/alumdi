import * as React from "react";
import Link from "gatsby-link";
import { compose } from "recompose";
import styled from "styled-components";
import mixins from "../styles/mixins";
var banner = require("../img/banner.jpg");

// Please note that you can use https://github.com/dotansimha/graphql-code-generator
// to generate all types from graphQL schema
interface Props {
  className: string;
  data: {
    site: {
      siteMetadata: {
        title: string;
      };
    };
  };
}

interface EnhancedProps extends Props {}

const enhance = compose<EnhancedProps, Props>();

const Navbar: React.SFC<EnhancedProps> = function({ className }) {
  return (
    <div className={`sticky-top ${className}`}>
      <div className="container">
        <div className="row">
          <div className="col-12">
            <nav className="navbar navbar-expand-md justify-content-between">
              <a
                className="navbar-brand d-inline-flex align-items-center"
                href="/"
              >
                <span className="">Blessing & Emeka</span>
              </a>
              <button
                className="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarNav"
                aria-controls="navbarNav"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="fas fa-bars" />
              </button>
              <div
                className="collapse navbar-collapse justify-content-end"
                id="navbarNav"
              >
                <ul className="navbar-nav">
                  <li className="nav-item active">
                    <a className="nav-link" href="/#our-story">
                      Our Story
                    </a>
                  </li>
                  <li className="nav-item active">
                    <a className="nav-link" href="/#events">
                      Events
                    </a>
                  </li>
                  <li className="nav-item active">
                    <a className="nav-link" href="/#gallery">
                      Gallery
                    </a>
                  </li>
                  <li className="nav-item active">
                    <a className="nav-link" href="/#honeymoon">
                      Honeymoon
                    </a>
                  </li>
                </ul>
              </div>
            </nav>
          </div>
        </div>
      </div>
    </div>
  );
};
const StyledNavbar = styled(Navbar)`
  z-index: 100;
  background: white;
  border-bottom: 1px solid #f2f4f4;
  .navbar {
    font-size: 0.75rem;
    display: flex;
    justify-content: center;
    align-items: stretch;
    padding-right: 0;
    padding-left: 0;
    .fa-navicon {
      color: black;
      font-size: 1.75em;
    }
    .navbar-logo {
      width: 50px;
      height: 50px;
      @include media-breakpoint-down(md) {
        width: 35px;
        height: 35px;
      }
    }
    .navbar-brand {
      text-transform: uppercase;
    }
    a {
      transition: all 0.375s;
      color: black;
      //color: black;
      &:hover {
        color: lightgray;
      }
    }
    .navbar-nav {
      .nav-item {
        .nav-link {
          padding-left: 1rem;
          padding-right: 1rem;
          text-transform: uppercase;
          @include regular-font-family();
        }
      }
    }
  }
`;

export default enhance(StyledNavbar);

import * as React from "react";
import Link from "gatsby-link";
import { compose, withState, withHandlers, lifecycle } from "recompose";
import styled from "styled-components";
import Img from "gatsby-image";
var banner = require("../img/banner.jpg");
declare var $: any;

// Please note that you can use https://github.com/dotansimha/graphql-code-generator
// to generate all types from graphQL schema
interface Props extends React.HTMLProps<HTMLDivElement> {
  // className?: string;
  body: React.Component<any, any>;
  doShowOnMount?: boolean;
  doShow: boolean;
  onHidden?: () => void;
}

interface State {
  isVisible: boolean;
}

class Modal extends React.PureComponent<Props, State> {
  private modal: any;

  constructor(props: Props) {
    super(props);
    this.modal = undefined;
    this.state = {
      isVisible: false
    };
    this.handleModalHidden = this.handleModalHidden.bind(this);
    this.handleModalShown = this.handleModalShown.bind(this);
  }

  componentDidMount() {
    const { doShowOnMount, doShow } = this.props;
    if (this.modal) {
      $(this.modal).modal({
        show: doShowOnMount || doShow,
        keyboard: true,
        backdrop: "static"
      });
      $(this.modal).on("hide.bs.modal", this.handleModalHidden);
      $(this.modal).on("hidden.bs.modal", this.handleModalHidden);
      $(this.modal).on("show.bs.modal", this.handleModalShown);
      $(this.modal).on("shown.bs.modal", this.handleModalShown);
    }
  }

  componentDidUpdate() {
    const { doShow } = this.props;
    const { isVisible } = this.state;
    if (!isVisible && doShow) {
      $(this.modal).modal("show");
    } else if (isVisible && !doShow) {
      $(this.modal).modal("hide");
    }
  }

  handleModalHidden() {
    const { onHidden } = this.props;
    this.setState(
      Object.assign({}, this.state, {
        isVisible: false
      })
    );
    if (onHidden) {
      onHidden();
    }
  }

  handleModalShown() {
    const { onHidden } = this.props;
    this.setState(
      Object.assign({}, this.state, {
        isVisible: true
      })
    );
  }

  public render() {
    const { className, body } = this.props;
    return (
      <div
        className={`modal ${className}`}
        tabIndex="-1"
        role="dialog"
        ref={c => (this.modal = c)}
      >
        <div
          className="modal-dialog modal-dialog-centered modal-lg"
          role="document"
        >
          <div className="modal-content rounded-0">
            <div className="modal-header">
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span className="fas fa-times" aria-hidden="true" />
              </button>
            </div>
            {body}
          </div>
        </div>
      </div>
    );
  }
}

export default styled(Modal)`
  display: block;
  height: 0;
  background: black;
  &.show {
    height: auto;
  }
  .modal-dialog {
    max-width: 100%;
    margin: auto;
    .modal-content {
      background: none;
      color: white;
      height: 50%;

      .modal-header {
        border-bottom: none;
        .close {
          color: white;
          opacity: 1;
        }
      }
    }
  }
`;

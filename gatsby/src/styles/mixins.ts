import { css } from "styled-components";

export default {
  mediaBreakpointDownExtraSmall(...args) {
    return css`
      @media (max-width: 575.98px) {
        ${css(...args)};
      }
    `;
  },
  mediaBreakpointDownSmall(...args) {
    return css`
      @media (max-width: 767.98px) {
        ${css(...args)};
      }
    `;
  },
  mediaBreakpointDownMedium(...args) {
    return css`
      @media (max-width: 991.98px) {
        ${css(...args)};
      }
    `;
  },
  mediaBreakpointDownLarge(...args) {
    return css`
      @media (max-width: 1199.98px) {
        ${css(...args)};
      }
    `;
  }
};

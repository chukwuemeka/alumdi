resource "aws_route53_zone" "default" {
  name = "${var.site_domain}"
}

resource "aws_route53_record" "cloudfront_alias" {
  zone_id = "${aws_route53_zone.default.zone_id}"
  name = ""
  type = "A"
  alias {
    name = "${aws_cloudfront_distribution.default.domain_name}"
    zone_id = "${aws_cloudfront_distribution.default.hosted_zone_id}"
    evaluate_target_health = false
  }
}


resource "aws_route53_record" "cname" {
  count = "${length(var.cname_records)}"
  zone_id = "${aws_route53_zone.default.id}"
  name    = "${element(keys(var.cname_records), count.index)}"
  type    = "CNAME"
  ttl     = "600"
  records = ["${lookup(var.cname_records, element(keys(var.cname_records), count.index))}"]
}

environment = "production"
s3_bucket_name = "alumdi-production-gatsby"
site_domain = "blessingandemeka.com"

cname_records = {
  "www" = "blessingandemeka.com"
  "_25c821103df07d1d99750750a6cfec50.blessingandemeka.com" = "_9ea631859a33a299f6c411433032d686.acm-validations.aws"
}

txt_records = {
  "_amazonses.baetoken.com" = "cq//UZ9jXfFshae3chUJAYTdRZTwcajn7TW5ei4yCx4="
  "baetoken.com" = "amazonses:cq//UZ9jXfFshae3chUJAYTdRZTwcajn7TW5ei4yCx4="
}


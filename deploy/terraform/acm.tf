data "aws_acm_certificate" "default" {
  domain   = "blessingandemeka.com"
  statuses = ["ISSUED"]
}

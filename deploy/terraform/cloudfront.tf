resource "aws_cloudfront_origin_access_identity" "default" {
  comment = "${var.app_name}"
}

resource "aws_cloudfront_distribution" "default" {
  enabled = true
  price_class = "PriceClass_All"
  aliases = [
    "${var.site_domain}",
    "www.${var.site_domain}"
  ]

  viewer_certificate {
    acm_certificate_arn = "${data.aws_acm_certificate.default.arn}"
    ssl_support_method = "sni-only"
    minimum_protocol_version = "TLSv1"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags {
    Name = "${var.app_name}"
    Environment = "${var.environment}"
    Project = "${var.project_name}"
  }

  origin {
    domain_name = "${aws_s3_bucket.default.website_endpoint}"
    origin_id = "${aws_s3_bucket.default.website_endpoint}"
    custom_origin_config {
        http_port = "80"
        https_port = "443"
        origin_protocol_policy = "http-only"
        origin_ssl_protocols = [
//        "SSLv3",
        "TLSv1",
        "TLSv1.1",
        "TLSv1.2",
      ]

    }
  }

  default_cache_behavior {
    allowed_methods = [
      "GET",
      "HEAD"]
    cached_methods = [
      "GET",
      "HEAD"]
    target_origin_id = "${aws_s3_bucket.default.website_endpoint}"
    compress = true

    forwarded_values {
      query_string = true
      cookies {
        forward = "all"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl = 0
    default_ttl = 31557600
    max_ttl = 31557600
  }
}


output "CLOUDFRONT_DISTRIBUTION_ID" {
  value = "${aws_cloudfront_distribution.default.id}"
}
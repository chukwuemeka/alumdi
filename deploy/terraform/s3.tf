data "aws_iam_policy_document" "s3_bucket" {
    statement {
    principals {
      identifiers = [
        "*",
      ]
      type = "*"
    }
    actions = [
      "s3:GetObject"
    ]
    resources = [
      "arn:aws:s3:::${var.s3_bucket_name}/*",
    ]
  }
}


resource "aws_s3_bucket" "default" {
  bucket = "${var.s3_bucket_name}"
  acl = "private"
  policy = "${data.aws_iam_policy_document.s3_bucket.json}"
  website {
    index_document = "index.html"
    error_document = "404.html"
  }

  cors_rule {
    allowed_methods = [
      "GET"]
    allowed_origins = [
      "*"]
    allowed_headers = [
      "Authorization"]
  }
  tags {
    Name = "${var.app_name}"
    Environment = "${var.environment}"
    Project = "${var.project_name}"
  }
}

output "S3_BUCKET_ID" {
  value = "${aws_s3_bucket.default.id}"
}
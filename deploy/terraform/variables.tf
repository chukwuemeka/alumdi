variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "environment" {}
variable "aws_region" {
  default = "us-east-1"
}
variable "aws_availability_zone" {
  default = "us-east-1a"
}

variable "api_name" {
  default = "Alumdi"
}
variable "app_name" {
  default = "alumdi"
}
variable "project_name" {
  default = "Alumdi"
}
variable "s3_bucket_name" {}

variable "site_domain" {
}

variable "txt_records" {
  type = "map"
}

variable "cname_records" {
  type = "map"
}


